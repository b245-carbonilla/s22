//console.log("Hello World");

//Non-Mutator Methods

/*
- Non-mutator methods are functions that do not modify or change an array after they have been created
- These methods do not manipulate the original array
- These methods perform tasks such as returning (or getting) elements from an array, combining arrays, and printing the output
- indexOf(), lastIndexOf(), slice(), toString(), concat(), join()
*/
console.log('%c Non-Mutator Methods', 'font-weight:bold;')

let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];
console.log(countries);
console.log("")

//indexOf()
/*
- Returns the index number of the "first matching element" found in an array
- if no match has been found, the result will be -1
- The search process will be done from the first element proceeding to the last element.
- Syntax:
	arrayName.indexOf(SearchValue);
	arrayName.indexOf(SearchValue, from Index);
*/

console.log('%c indexOf Method:', 'background: #89CFF0')
let firstIndex = countries.indexOf('PH');
console.log('Result of indexOf method: ' + firstIndex); // 1

let invalidCountry = countries.indexOf('BR');
console.log('Result of indexOf Method: ' + invalidCountry);
console.log("") // -1

//lastIndexOf()
/*
- Returns the index number of the "last matching element" found in an array
- The search process will be done from the last element proceeding to the first element.
- Syntax:
	arrayName.lastIndexOf(SearchValue);
	arrayName.lastIndexOf(SearchValue, fromIndex);
*/
console.log('%c lastIndexOf Method:', 'background: #89CFF0')

//Getting the index number starting from the last element
let lastIndex = countries.lastIndexOf('PH');
console.log('Result of lastIndexOf method: ' + lastIndex); // 5

//Getting the index number starting from a specified index
let lastIndexStart = countries.lastIndexOf('PH', 6);
let lastIndexStarter = countries.lastIndexOf('PH', 4);
console.log('Result of lastIndexOf method: ' + lastIndexStart); // 5
console.log('Result of lastIndexOf method: ' + lastIndexStarter); // 1

//slice ()
/*
- slices elements from an array and returns a new array
- Syntax: arrayName.slice(startingIndex);
		  arrayName.slice(startingIndex, endingIndex);
*/
console.log('%c slice Method:', 'background: #89CFF0')
//Slicing off elements from a specified index to the last element
let sliceArrayA = countries.slice(2);
console.log("Result from slice method: ");
console.log(sliceArrayA); // starts with CAN, SG, TH

//Slicing of elements from a specified inex to another index
let slicedArrayB = countries.slice(2, 4)
console.log("Result from slice method: ")
console.log(slicedArrayB);

//Slicing of elements from a specified inex to another index
console.log("Negative Values: ")
let slicedArrayC = countries.slice(-4, -1)
let slicedArrayD = countries.slice(-3)
let slicedArrayE = countries.slice(-5)
console.log("Result from slice method: ")
console.log(slicedArrayC); //DE
console.log(slicedArrayD); // PH, FR, DE
console.log(slicedArrayE); // SG, TH, PH, FR, DE
console.log(" ")


//toString()
/*
- Returns an array as a string separated by commas
- Syntax: 
	arrayName.toString();
		  
*/
console.log('%c toString Method:', 'background: #89CFF0')
let stringArray = countries.toString();
console.log("Result from toString method: ");
console.log(stringArray);
console.log("")

//concat() 
/*
- Combines two arrays and returns the combined result
- Syntax: 
	arrayName.concat(arrayB);
	arrayName.concat(elementA, elementB);

		  
*/
console.log('%c concat Method:', 'background: #89CFF0')
let taskArrayA = ['drink HTML', "eat javascript"];
let taskArrayB = ['inhale CSS', "breath sass"];
let taskArrayC = ["get got", "be node"];

let tasks = taskArrayA.concat(taskArrayB);
console.log("Result from concat method: ");
console.log(tasks);

//Combining multiple aarrays
console.log("Result from concat method: ");
let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);

console.log("Rearranged:")
let allTasksA = taskArrayB.concat(taskArrayC, taskArrayA);
console.log(allTasksA);

//Combining arrays with elements
let combinedTasks = taskArrayA.concat('smell express', 'throw react');
console.log("Results from concat method: ");
console.log(combinedTasks);
console.log("")

//join()
/*
- Returns an array as a string separated by specified separator string
- Syntax: 
	arrayName.join("separationString");
		 
*/
console.log('%c join Method:', 'background: #89CFF0')
let users = ['John', 'Jane', 'Joe', 'Robert'];
console.log(users.join());
console.log(users.join(" "));
console.log(users.join(' - '));
console.log(users.join(' & '));

console.log(" ")

console.warn("--end of Non-Mutator Methods--");

console.log('%c Iteration Methods', 'font-weight: bold;')

//Iteration Methods

/*
- Iteration Methods are loops design to perform repetitive tasks on arrays 
- Useful for manipulating array data resulting in complex tasks.
*/

// foreach()
/*
- similar to the for loop that iterates on each array element
- Variable names for arrays are usually written in the plural form of the data stored in an array
- It is a common practice to use the singular form of the array content for parameter names used in array loops. 
- Array iteration methods normally work with a function supplied as an argument
- Syntax: 
	arrayName.forEach(function(individualElement) {statement} )
*/

allTasks.forEach(function(task) {
		console.log(task)
});

// forEach is making the array to become a loop by used of function
console.log('%c forEach Method:', 'background: #89CFF0')

let student = ['Jeru', 'Anjanette', 'Glyn', 'Jake', 'Gabryl', 'Daniel']
student.forEach(function(x) {
		console.log("Hi, "+ x + " !");
});

console.log("")

//Using forEach with conditional statements
console.log("%c Filtered forEach:", 'background: #89CFF0');

let filteredTasks = []

allTasks.forEach(function(task) {

		if (task.length >= 10) {
			console.log(task)
			filteredTasks.push(task);
		}
});

console.log("Result of filteredTasks:");
console.log(filteredTasks);

student.forEach(function(student) {
	if (student.length <= 4) {
		console.log("Sa kanan ng room uupo si " + student);
	} else {
		console.log("Sa kaliwa ng room uupo si " + student);
	}
});

console.log("")
// map()
/*
- It iterates of each element AND returns new array with different values depending on the result of the function's operation.
- this is useful performing tasks where mutating/changing the elements are required
- Unlike forEach method, the map method requires the use of a "return" statement in order to create another array with the performed operation.
- Syntax:
let / const resultArray = arrayName.map(function(individualElement))
*/
console.log("%c map() Method:", 'background: #89CFF0');

let numbers = [1, 2, 3, 4, 5];
console.log("Before the map method: " + numbers);

let numberMap = numbers.map(function(number) {
	return number * number
})

console.log("Results/After map method:");
console.log(numberMap);
console.log("")

// every()
/*
- Checks if all element in an array meet the given condition
- This is useful for validating data stored in arrays especially when dealing with large amounts of data.
- Returns the true value if all elements meet the condition and false if otherwise
- Syntax:
	let / const resultArray= arrayName.every(function(element) {
		return expression / condition
	})
*/

console.log("%c every() Method:", 'background: #89CFF0');

let allValid = numbers.every(function(number) {
	return(number < 3);
});
console.log("Result of every method:");
console.log(allValid);
console.log("")


// some()
/*
- Checks if atleast one element in the array meets the given condition
- Returns a true value if atleast one elements meet the condition and false if otherwise
- Syntax:
	let / const resultArray= arrayName.some(function(element) {
		return expression / condition
	})
*/
console.log("%c some() Method:", 'background: #89CFF0');


let someValid = numbers.some(function(number) {
	return (number < 2);
});

console.log("Result of some method: ");
console.log(someValid);

// Combining the returned result from the every/some method may be used in other statements to perform consecutives.
if (someValid) { // truthy or faulty / == true
	console.log("Some numbers in the array are greater than 2")
}
console.log("")

// filter()
/*
- Returns new array that contains elements which meets the given condition
- Returns an empty array if no elements are found
- Useful for filtering array elements with a given condition and shortens the syntax compared to using other array iteration methods.
- Syntax:
	let / const resultArray= arrayName.filter(function(element) {
		return expression / condition
	})
*/

console.log("%c filter() Method:", 'background: #89CFF0');
let filterValid = numbers.filter(function(number) {
	return (number < 3);
});

console.log("Result of filter method: ");
console.log(filterValid);


// No element found
let nothingFound = numbers.filter(function(number) {
	return (number = 0)
});
console.log(nothingFound);


//filtering using forEach
 let filteredNumber = [];

 numbers.forEach(function(number) {
 	console.log(number)

 	if (number < 3) {
 		filteredNumber.push(number)
 	}
 });

 console.log("Result of filter method:")
 console.log(filteredNumber)
 console.log("")


// includes() method
/*
 - Methods can be "chained" by using them one after another
 - The result of the first method is used on the second method until all chained methods have been solved.
*/
 console.log("%c includes() Method:", 'background: #89CFF0');

 let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

 let filteredProducts = products.filter(function(product) {
 	return product.toLowerCase().includes("a"); 

 // includes() useful in unit testing
 });

 console.log(filteredProducts);
 console.log("")

 //reduce()
 /*
 - Evaluates statements from left to right and returns / reduces the array into a single value.
 - Syntax:
  let / const resultArr = arrayName. reduce(function(accumulator,currentValue) {
		return expression / condition
  })
 - The "accumulator" parameter in the function stores the result for every iteration of the loop
 - The "currentValue" is the current / next element in the array that is evaluated in each iteration of the loop
 - How the "reduce" method works:
 	1. The first/result element in the array is stores in the "accumulator" parameters
 	2. The second/next element in the array is stores in the "currentValue"
 	3. An operation is performed on the two lements
 	4. The loop  repeats steps 1 to 3 until all elements have been worked on.
 */
console.log("%c reduce() Method:", 'background: #89CFF0')

 let iteration = 0;

 let reduceArray = numbers.reduce(function(x, y) {
 	console.warn('current iteration: ' + ++iteration);
 	console.log('accumulator: ' + x)
 	console.log('currentValue: ' + x)

 	// The operation to reduce the array into a single value
 	return x + y
 });

console.log('Result of reduce method: ' + reduceArray);

//Reducing string arrays
let list = ['Hello', 'Again', 'World'];

let reducedJoin = list.reduce(function(x, y) {
	return x + ' ' + y;
});

console.log("Result of reduce method: " + reducedJoin);